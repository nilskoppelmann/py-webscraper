# imports
from bs4 import BeautifulSoup
import requests
import re
from collections import Counter

# this function returns a soup page object
def getPage(url):
    r = requests.get(url)
    data = r.text
    spobj = BeautifulSoup(data, 'lxml')
    return spobj

# scraper website: heise.de
def main():

    popularWords = []
    popularNouns = []

    for page in range(0, 10):

        url = 'https://www.heise.de/thema/https?seite=' + str(page)

        content = getPage(url).find('div', {'class': 'keywordliste'})

        if content is None: continue

        content = content.find('aside', {'class': 'recommendations'})
        content = content.findAll('div', {'class': 'recommendation'})

        for c in content:
            title = c.find('header').text.encode('utf-8')   # get title
            title = re.sub('[^a-zA-Z0-9-_*.\ ]', '', title) # remove all special chars

            for word in title.split(' '):

                if len(word) == 0: continue

                popularWords.append(word)

                if word.istitle() or word.isupper():
                    popularNouns.append(word)

    print('\nDone! All titles scraped.\n')

    countWords = Counter(popularWords)
    countNouns = Counter(popularNouns)

    print('the most 3 common words: ' + str(countWords.most_common(3)))
    print('the most 3 common nouns: ' + str(countNouns.most_common(3)))

# main program

if __name__ == '__main__':
    main()